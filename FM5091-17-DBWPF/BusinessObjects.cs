﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FM5091_17_DBWPF
{
    public class BusinessObject
    {
        public virtual void SaveToSQL() { }
    }

    public class Customer : BusinessObject
    {
        public override void SaveToSQL()
        {
            base.SaveToSQL();
        }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
    }

    public class Transaction : BusinessObject
    {
        public override void SaveToSQL()
        {
            base.SaveToSQL();
        }
    }
}
